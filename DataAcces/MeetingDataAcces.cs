﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using darwin_outlook_model;
using MongoDB.Bson;
using MongoDB.Driver;


namespace Darwin_Booking.DataAcces
{
    public class MeetingDataAcces
    {
        MongoClient _client;
        IMongoDatabase _db;

        public MeetingDataAcces()
        {
            _client = new MongoClient("mongodb://0.tcp.ngrok.io:10442");
            _db = _client.GetDatabase("darwin");
        }

        public IEnumerable<MeetingModel> GetMeetingModels()
        {
            try
            {
                return _db.GetCollection<MeetingModel>("MeetingModels").Find(_ => true).ToList();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        public async Task<MeetingModel> GetMeetingModel(ObjectId id)
        {
            var x = await _db.GetCollection<MeetingModel>("MeetingModels").Find(p => ObjectId.Parse(p.Id) == id).SingleAsync();
            
            return x;
        }

        public async Task<MeetingModel> Create(MeetingModel p)
        {
            try
            {
                await _db.GetCollection<MeetingModel>("MeetingModels").InsertOneAsync(p);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return p;
        }

        public async void Update(ObjectId id, MeetingModel p)
        {
            p.Id = id.ToString();
            await _db.GetCollection<MeetingModel>("MeetingModels").ReplaceOneAsync(item => item.Id == p.Id, p, new UpdateOptions { IsUpsert = true });

        }
        public async void Remove(ObjectId id)
        {
            await _db.GetCollection<MeetingModel>("MeetingModels").DeleteOneAsync(item => ObjectId.Parse(item.Id) == id);
        }
    }
}