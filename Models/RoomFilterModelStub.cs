﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using darwin_outlook_model.CreateMeeting.RoomFilters;

namespace darwin_booking.Models
{
    public class RoomFilterModelStub
    {
            public static readonly RoomSizeChoice[] RoomSizeChoices= new RoomSizeChoice[]
            {
                new RoomSizeChoice()
                { 
                    Key= "Room6",
                    Name= "6 people",
                    AmountOfPeople = 6
                },
                new RoomSizeChoice
                {
                    Key= "Room8",
                    Name= "8 people",
                    AmountOfPeople = 8
                },
                new RoomSizeChoice
                {
                    Key= "Room10",
                    Name= "10 people",
                    AmountOfPeople = 10
                },
                new RoomSizeChoice
                {
                    Key= "Room12",
                    Name= "12 people",
                    AmountOfPeople = 12
                },
                new RoomSizeChoice
                {
                    Key= "Room22",
                    Name= "22 people",
                    AmountOfPeople = 22
                },
            };

        public static readonly RoomEquipmentChoice[] RoomEquipmentChoice = new RoomEquipmentChoice[]
        {
            new RoomEquipmentChoice
            {
                Key = "EquipmentPresentation",
                Name= "Presentation",
            },
            new RoomEquipmentChoice
            {
                Key = "EquipmentVideoConference",
                Name= "VideoConference",
            },
            new RoomEquipmentChoice
            {
                Key = "EquipmentPhone",
                Name= "Phone",
            },
        };
    }
   


   
    
}
