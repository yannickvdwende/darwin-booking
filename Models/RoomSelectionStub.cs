﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using darwin_outlook_model.CreateMeeting.RoomSelection;

namespace darwin_booking.Models
{
    public class RoomSelectionStub
    {
        public static List<RoomSelectionChoice> RoomSelectionChoices()
        {
            var result = new List<RoomSelectionChoice>
            {
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 1",
                    Checked = false,
                    Key = "Ruimte1@darwinVDLTestNovember2018.onmicrosoft.com",
                    AmountOfPeople = 6,
                    Location = "Den Bosch",
                    MailAdress = "Ruimte1@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>
                    {
                        new RoomAdditions()
                        {
                            Name = "Coffee",
                            Key = "Coffee",
                            Icon = "",
                            MinimumPreOrdertime = TimeSpan.FromHours(0.5),
                            NeedsToBePreOrdered = true
                        },
                        new RoomAdditions()
                        {
                            Name = "Lunch",
                            Key = "Lunch",
                            Icon = "",
                            MinimumPreOrdertime = TimeSpan.FromHours(0.5),
                            NeedsToBePreOrdered = true
                        },
                    },
                    Equipment = new List<RoomEquipment>
                    {
                        new RoomEquipment
                        {
                            Name = "Presentation",
                            Key = "EquipmentPresentation",
                            Icon = "",
                            InRoom = true,
                            NeedsToBeReserved = false,
                        },
                        new RoomEquipment
                        {
                            Name = "VideoConference",
                            Key = "EquipmentVideoConference",
                            Icon = "",
                            InRoom = true,
                            NeedsToBeReserved = false,
                        },
                    },
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 2",
                    Checked = false,
                    AmountOfPeople = 8,
                    Key = "Ruimte2@darwinvdltestnovember2018.onmicrosoft.com",
                    Location = "Den Bosch",
                    MailAdress = "Ruimte2@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>
                    {
                        new RoomAdditions()
                        {
                            Name = "Coffee",
                            Key = "Coffee",
                            Icon = "",
                            MinimumPreOrdertime = TimeSpan.FromHours(0.5),
                            NeedsToBePreOrdered = true
                        },
                        new RoomAdditions()
                        {
                            Name = "Lunch",
                            Key = "Lunch",
                            Icon = "",
                            MinimumPreOrdertime = TimeSpan.FromHours(0.5),
                            NeedsToBePreOrdered = true
                        },
                    },
                    Equipment = new List<RoomEquipment>
                    {
                        new RoomEquipment
                        {
                            Name = "Presentation",
                            Key = "EquipmentPresentation",
                            Icon = "",
                            InRoom = true,
                            NeedsToBeReserved = false,
                        },
                        new RoomEquipment
                        {
                            Name = "VideoConference",
                            Key = "EquipmentVideoConference",
                            Icon = "",
                            InRoom = true,
                            NeedsToBeReserved = false,
                        },
                    },
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 3",
                    Checked = false,
                    Key = "Ruimte3@darwinvdltestnovember2018.onmicrosoft.com",
                    AmountOfPeople = 10,
                    Location = "Den Bosch",
                    MailAdress = "Ruimte3@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>
                    {
                        new RoomAdditions()
                        {
                            Name = "Coffee",
                            Key = "Coffee",
                            Icon = "",
                            MinimumPreOrdertime = TimeSpan.FromHours(0.5),
                            NeedsToBePreOrdered = true
                        },
                    },
                    Equipment = new List<RoomEquipment>
                    { 
                        new RoomEquipment
                        {
                            Name = "VideoConference",
                            Key = "EquipmentVideoConference",
                            Icon = "",
                            InRoom = true,
                            NeedsToBeReserved = false,
                        },
                    },
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 4",
                    Checked = false,
                    Key = "Ruimte4@darwinvdltestnovember2018.onmicrosoft.com",
                    AmountOfPeople = 20,
                    Location = "Den Bosch",
                    MailAdress = "Ruimte4@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>
                    {
                        new RoomAdditions()
                        {
                            Name = "Coffee",
                            Key = "Coffee",
                            Icon = "",
                            MinimumPreOrdertime = TimeSpan.FromHours(0.5),
                            NeedsToBePreOrdered = true
                        },
                    },
                    Equipment = new List<RoomEquipment>
                    {
                        new RoomEquipment
                        {
                            Name = "Presentation",
                            Key = "EquipmentPresentation",
                            Icon = "",
                            InRoom = true,
                            NeedsToBeReserved = false,
                        },
                    },
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 5",
                    Checked = false,
                    Key = "Ruimte5@darwinvdltestnovember2018.onmicrosoft.com",
                    Location = "Den Bosch",
                    AmountOfPeople = 8,
                    MailAdress = "Ruimte4@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>
                    {
                        new RoomAdditions()
                        {
                            Name = "Coffee",
                            Key = "Coffee",
                            Icon = "",
                            MinimumPreOrdertime = TimeSpan.FromHours(0.5),
                            NeedsToBePreOrdered = true
                        },
                    },
                    Equipment = new List<RoomEquipment>
                    {
                        new RoomEquipment
                        {
                            Name = "Presentation",
                            Key = "EquipmentPresentation",
                            Icon = "",
                            InRoom = true,
                            NeedsToBeReserved = false,
                        },
                    },
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 6",
                    Checked = false,
                    Key = "Ruimte6@darwinvdltestnovember2018.onmicrosoft.com",
                    AmountOfPeople = 30,
                    Location = "Den Bosch",
                    MailAdress = "Ruimte6@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>(),
                    Equipment = new List<RoomEquipment>(),
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 7",
                    Checked = false,
                    Key = "Ruimte7@darwinvdltestnovember2018.onmicrosoft.com",
                    AmountOfPeople = 40,
                    Location = "Den Bosch",
                    MailAdress = "Ruimte7@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>(),
                    Equipment = new List<RoomEquipment>(),
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 8",
                    Checked = false,
                    Key = "Ruimte8@darwinvdltestnovember2018.onmicrosoft.com",
                    AmountOfPeople = 6,
                    Location = "Den Bosch",
                    MailAdress = "Ruimte8@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>(),
                    Equipment = new List<RoomEquipment>(),
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 9",
                    Checked = false,
                    Key = "Ruimte9@darwinvdltestnovember2018.onmicrosoft.com",
                    AmountOfPeople = 8,
                    Location = "Den Bosch",
                    MailAdress = "Ruimte9@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>(),
                    Equipment = new List<RoomEquipment>(),
                },
                new RoomSelectionChoice()
                {
                    Name = "Ruimte 10",
                    Checked = false,
                    Key = "Ruimte10@darwinvdltestnovember2018.onmicrosoft.com",
                    AmountOfPeople = 10,
                    Location = "Den Bosch",
                    MailAdress = "Ruimte10@darwinvdltestnovember2018.onmicrosoft.com",
                    Additions = new List<RoomAdditions>(),
                    Equipment = new List<RoomEquipment>(),
                },
            };

            return result;
        }



    }
}
