﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using darwin_outlook_model;
using Darwin_Booking.DataAcces;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Darwin_Booking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MeetingController : ControllerBase
    {
        // GET
        // GET api/Meeting
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // POST api/Meeting
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] MeetingModel model)
        {
            var mda = new MeetingDataAcces();

            try
            {
                await mda.Create(model);
            }
            catch (Exception ex)
            {

                var x = ex.Message;
            }

            return Ok();
        }

        // PUT api/Meeting/
        [HttpPut]
        public void Put([FromBody] MeetingModel model)
        {
            var mda = new MeetingDataAcces();

            try
            {
                mda.Update(ObjectId.Parse(model.Id), model);
            }
            catch (Exception ex)
            { 
                var x = ex.Message;
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}