﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using darwin_booking.Models;
using Microsoft.AspNetCore.Mvc;

namespace darwin_booking_stub.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomFilterController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Hello from darwin_booking_stub";
        }

        //get API/RoomFilter/GetPossibleRoomFilterSizes/6
        [HttpGet("/api/RoomFilter/GetPossibleRoomFilterSizes/{minimumSize}")]
        public IActionResult GetPossibleRoomFilterSizes(int minimumSize)
        {
            var result = RoomFilterModelStub.RoomSizeChoices.Where(x => x.AmountOfPeople >= minimumSize).ToList();
            return new ObjectResult(result);
        }

        //get API/values/GetPossibleRoomFilterEquipment/
        [HttpGet("/api/RoomFilter/GetPossibleRoomFilterEquipment")]
        public IActionResult GetPossibleRoomFilterEquipment()
        {
            return new ObjectResult(RoomFilterModelStub.RoomEquipmentChoice);
        }
    }
}
