﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using darwin_booking.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using darwin_outlook_model.CreateMeeting;
using darwin_outlook_model.CreateMeeting.RoomFilters;
using darwin_outlook_model.CreateMeeting.RoomSelection;
namespace darwin_booking_stub.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class RoomSelectionController : ControllerBase
    {
        [HttpPost("/api/RoomSelection/GetPossibleRoomSelectionChoices")]
        public IActionResult GetPossibleRoomSelectionChoices([FromBody]RoomFilter filter)
        {
            var result = RoomSelectionStub.RoomSelectionChoices();
            var amountOfPeople = filter.RoomSizes.Single(roomSize => roomSize.Checked == true).AmountOfPeople;
            var selectedFilterEquipmentKeys = filter.RoomEquipmentChoices.Where(filterEquipment => filterEquipment.Checked).Select(filterEquipment => filterEquipment.Key).ToList();
            var selectedFilterAdditionKeys = filter.RoomAdditionChoices.Where(filterAdditions => filterAdditions.Checked).Select(filterAdditions => filterAdditions.Key).ToList();

            var roomSelectionChoices = result.Where(roomChoice =>
                roomChoice.AmountOfPeople >= amountOfPeople
                && (selectedFilterEquipmentKeys.Count == 0 || roomChoice.Equipment.Any(rcEquipment => selectedFilterEquipmentKeys.Contains(rcEquipment.Key)))
                && (selectedFilterAdditionKeys.Count == 0 || roomChoice.Additions.Any(rcAddition => selectedFilterAdditionKeys.Contains(rcAddition.Key)))
            ).ToList();
         
            return new ObjectResult(roomSelectionChoices);
        }
    }
}