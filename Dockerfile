FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 51536
EXPOSE 44378

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ../Darwin-Booking/Darwin-Booking.csproj ../Darwin-Booking/
RUN dotnet restore ../Darwin-Booking/Darwin-Booking.csproj
COPY . .
WORKDIR /src/../Darwin-Booking
RUN dotnet build Darwin-Booking.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish Darwin-Booking.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Darwin-Booking.dll"]
